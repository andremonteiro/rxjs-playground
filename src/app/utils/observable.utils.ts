import { Observable } from 'rxjs';
import { Logger } from '../services/logger/logger.service';

export class ObservableUtils {
  public static createColdObservable(obs: any, logger: Logger) {
    return new Observable(subscriber => {
      logger.log('CREATING COLD OBSERVABLE!');
      const endpoint = obs;
      const s = endpoint.subscribe({
        next: d => subscriber.next(d),
        error: e => subscriber.error(e),
        complete: () => subscriber.complete()
      });

      return () => {
        s.unsubscribe();
      };
    });
  }
}
