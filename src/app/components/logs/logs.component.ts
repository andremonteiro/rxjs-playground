import { Component, OnInit } from '@angular/core';
import { Logger } from 'src/app/services/logger/logger.service';

@Component({
  selector: 'app-logs',
  styles: [
    `
      .logs {
        max-height: 70vh;
        width: 100%;
        overflow: auto;
      }
    `
  ],
  template: `
    <h2>Logs</h2>
    <hr />
    <div class="logs" [innerHTML]="logger.logs"></div>
  `
})
export class LogsComponent implements OnInit {
  constructor(public logger: Logger) {}

  ngOnInit() {}
}
