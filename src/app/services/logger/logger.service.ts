import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class Logger {
  public logs = '';

  constructor() {}

  private appendLine(params, style?) {
    const timestamp = new Date().toLocaleTimeString();
    let line = `<span class="${style}">[${timestamp}]&nbsp;&nbsp;&nbsp;`;

    params.forEach(p => {
      line += p + ' ';
    });

    this.logs += line + '</span><hr class="mt-1 mb-1">';
  }

  log(...params) {
    this.appendLine(params);
    console.log(...params);
  }

  error(...params) {
    this.appendLine(params, 'text-danger');
    console.error(...params);
  }

  warn(...params) {
    this.appendLine(params, 'text-warning');
    console.warn(...params);
  }

  info(...params) {
    this.appendLine(params, 'text-info');
    // tslint:disable-next-line: no-console
    console.info(...params);
  }

  clear() {
    this.logs = '';
  }
}
