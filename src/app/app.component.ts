import { Component, OnInit } from '@angular/core';
import {
  Observable,
  interval,
  of,
  throwError,
  zip,
  BehaviorSubject,
  ReplaySubject,
  timer,
  observable,
  Subject,
} from 'rxjs';
import {
  tap,
  shareReplay,
  map,
  catchError,
  take,
  timeout,
  switchMap,
  filter,
  flatMap,
  skipUntil,
  window,
  auditTime,
  audit,
  throttle,
  throttleTime,
  debounceTime,
  takeUntil,
  repeatWhen,
  retryWhen,
  delay,
} from 'rxjs/operators';
import { Logger } from './services/logger/logger.service';
import { ObservableUtils } from './utils/observable.utils';

interface AccessTokens {
  [key: string]: string;
}

interface Button {
  text: string;
  value: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  public window = window;
  public buttons: Button[] = [];

  constructor(private logger: Logger) {}

  /**
   * Choose here the function to be run
   */
  ngOnInit() {
    // this.coldObservables();
    // this.rxjsTrials();
    // this.baseUrlsTest();
    // this.splitStream();
    // this.taptap();
    // this.oruSwitchMap();
    // this.toPromiseVsGetValue();
    // this.completeTrial();
    // this.skipUntilTests();
    // this.filterTake();
    // this.takeUntilAndTimer();
    // this.coldObservableShareReplayUnsubscribe();
    // this.hotOrColdInterval();
    // this.coldObservableIllustration();
    // this.hotObservableIllustration();
    this.repeatWhenTest();
  }

  private addTriggerButton(text: string, value = false) {
    const button: Button = {
      text,
      value,
    };

    this.buttons.push(button);

    return button;
  }

  /**
   * What was tested:
   *    creating cold observables - observables that are only initialized
   *    when subscribed and their subscriptions share the values
   * How it was tested:
   *    1. Create a cold observable "o1"
   *    2. Create two subscriptions to "o1". Notice that two endpoints #1 were created.
   *    3. Create a cold observable "o2", and pipe it through a shareReplay operator.
   *    4. Create two subscriptions to "o2". Notice that only one endpoint #2 was created.
   */
  private coldObservables() {
    this.logger.info('coldObservables');

    let i = 0;

    const createColdObservable = (num) =>
      new Observable((subscriber) => {
        i++;
        const endpoint = interval(2000);
        this.logger.log('CREATED ENDPOINT ' + num);
        const s = endpoint.subscribe({
          next: (n) => subscriber.next(n),
          error: (e) => subscriber.error(e),
          complete: () => subscriber.complete(),
        });

        return () => {
          s.unsubscribe();
        };
      });

    const o1 = createColdObservable(1);

    const a = o1.subscribe(() => {
      this.logger.log('A');
    });
    const b = o1.subscribe(() => {
      this.logger.log('B');
    });

    const o2 = createColdObservable(2).pipe(shareReplay());
    const c = o2.subscribe(() => {
      this.logger.log('C');
    });
    const d = o2.subscribe(() => {
      this.logger.log('D');
    });
  }

  private rxjsTrials() {
    this.logger.info('rxjsTrials');

    const obj = {
      data: { fullPrivacy: false },
    };

    const sub = of(obj);
    // const sub = throwError(new Error('Any Error'));

    const o = ObservableUtils.createColdObservable(sub, this.logger).pipe(
      map((value: any) => value.data),
      map((privacyMode: { fullPrivacy: boolean }) => {
        this.logger.log('Map #2, privacyMode: ', privacyMode);

        if (!privacyMode || !(privacyMode.fullPrivacy !== undefined)) {
          throw new Error('No privacy mode.');
        }

        return privacyMode.fullPrivacy ? -1 : 1;
      }),
      catchError((err: Error) => {
        this.logger.error(
          `Subscription to /privacysetup/privacyModes/ failed. Error: ${err.message}`,
          'rsi-services MOD4 - privacysetup.service.ts - subscribeForData'
        );

        return throwError(err);
      }),
      shareReplay(1)
    );

    o.subscribe(
      (val) => {
        this.logger.log('Value: ' + val);
      },
      (err) => {
        this.logger.error('Error: ', err);
      }
    );
  }

  /**
   * What was tested:
   *    1. Data validation
   *    2. Error rethrowing
   *    3. Timeout operator
   */
  private baseUrlsTest() {
    this.logger.info('baseUrlsTest');

    const obj = {
      data: null,
    };

    const o = of(obj);
    // const o = of({});
    // const o = new Observable();

    o.pipe(
      timeout(5000),
      take(1),
      map((value) => {
        this.logger.log('[Map] value: ', value);

        if (value && value.data) {
          return value.data;
        }

        throw new Error('No data.');
      }),
      catchError((err: Error) => {
        this.logger.error('[CatchError] Error: ', err);

        return throwError(err);
      })
    ).subscribe(
      (value) => this.logger.log('Value: ', value),
      (error) => this.logger.error('Error: ', error)
    );
  }

  /**
   * What was tested: splitting a stream into two and then one again using switchMap and zip
   */
  private splitStream() {
    this.logger.info('splitStream');

    const o = of('11111').pipe(
      tap(this.logger.log),
      switchMap((val) => {
        const arr = [of('22222'), of('33333')];

        return zip(...arr);
      })
    );

    o.subscribe((val) => {
      this.logger.log(val);
    });
  }

  /**
   * What was tested: if tap runs after an error is thrown - it doesn't :)
   */
  private taptap() {
    this.logger.info('taptap');

    throwError('dis')
      .pipe(tap(() => this.logger.log('taptaptap')))
      .subscribe(
        () => {
          this.logger.log('sub');
        },
        (err) => {
          this.logger.log('err', err);
        }
      );
  }

  /**
   * What was tested: switchMap cancels previous observable, other ones dont (e.g. flatMap)
   * How it was tested:
   *    1. The observable sends automatically the value 1
   *    2. After 5 seconds it will send the value 2
   *    3. Wait until the two values reach switchMap or flatMap
   *    4. Click on the trigger button for it to continue
   *    5. Watch the results
   * How to reproduce: Choose one and comment out the other: switchMap or flatMap
   *
   */
  private oruSwitchMap() {
    this.logger.info('oruSwitchMap');

    // Triggers
    this.buttons.push({
      text: 'Signal interval to continue',
      value: false,
    });

    const obs = new BehaviorSubject<number>(1);

    setTimeout(() => {
      obs.next(2);
    }, 5000);

    obs
      .pipe(
        tap((val) => {
          this.logger.log('compose full url', val);
        }),
        // Choose here.
        switchMap((val) =>
          interval(1000).pipe(
            filter(() => this.buttons[0].value),
            take(1),
            map(() => val)
          )
        )
        // flatMap(val =>
        //   interval(1000).pipe(
        //     filter(() => this.buttons[0].value),
        //     take(1),
        //     map(() => val)
        //   )
        // )
      )
      .subscribe((val) => {
        this.logger.log('subscribe val:', val);
      });
  }

  /**
   * What was tested:
   *    BehaviorSubject has a method called getValue which synchronously provides the latest emitted value
   *    We wanted the "same" behavior with ReplaySubject, using async/await and toPromise
   * What was learned:
   *    For it to work, we have to put a take(1) in the pipe using ReplaySubject, or else it will be waiting
   *    for values until the subscription completes. take(1) completes it after receiving one value
   */
  private async toPromiseVsGetValue() {
    this.logger.info('toPromiseVsGetValue');

    const replay = new ReplaySubject(1);
    const behavior = new BehaviorSubject('val behavior');

    replay.next('val replay');

    this.logger.log('Behavior value:', behavior.getValue());

    // With take(1), subscription is completed automatically
    this.logger.log(
      'Replay value with take 1:',
      await replay.pipe(take(1)).toPromise()
    );

    // Without take(1), the subscription must complete
    replay.complete();

    this.logger.log('Replay value without take(1):', await replay.toPromise());
  }

  /**
   * What was tested: take(1) completes the subscription after receiving one value
   */
  private completeTrial() {
    this.logger.info('completeTrial');

    const replay = new ReplaySubject(1);
    replay.next('val replay');

    // take(1) completes subscription after receiving 1 value
    replay.pipe(take(1)).subscribe(
      (val) => this.logger.log('Value:', val),
      (err) => this.logger.log('Error:', err),
      () => {
        this.logger.log('Subscription complete');
      }
    );
  }

  private skipUntilTests() {
    const arr = [];

    for (let i = 0; i < 100; i++) {
      arr.push(i);
    }

    const getTimer = () => interval(3000);

    interval(1000)
      .pipe(
        switchMap(() => of(arr.shift())),
        tap((val) => {
          this.logger.log('interval emitted value: ', JSON.stringify(val));
        }),
        filter((val) => val % 3 === 0),
        tap((val) => this.logger.log('filtered: ', val)),
        throttleTime(3000)
        // auditTime(3010),
        // debounceTime(3200)
      )
      .subscribe((val) => {
        this.logger.log(
          '[subscribe] passed through skipUntil, value:',
          JSON.stringify(val)
        );
      });
  }

  private filterTake() {
    const arr = [];

    for (let i = 0; i < 100; i++) {
      arr.push(i);
    }
    this.logger.log('init');

    interval(1000)
      .pipe(
        switchMap(() => of(arr.shift())),
        tap((value) => {
          this.logger.log('tap value:', value);
        }),
        filter((value) => value >= 10),
        take(1)
      )
      .subscribe((value) => {
        this.logger.info('subscribe value:', value);
      });
  }

  private takeUntilAndTimer() {
    let cancelTimer = false;

    const timer$ = new Observable((subscriber) => {
      const t = timer(3000).pipe(
        filter(() => !cancelTimer),
        tap(() => this.logger.log('Timer ran out.'))
      );
      const subscription = t.subscribe(subscriber);

      return () => {
        subscription.unsubscribe();
        this.logger.log('Unsubscribed to the timer');
      };
    });

    interval(500)
      .pipe(takeUntil(timer$))
      .subscribe(
        () => {
          this.logger.log('Interval emitted.');
        },
        () => {},
        () => {
          this.logger.log('Interval completed.');
        }
      );

    timer(1500).subscribe(() => {
      cancelTimer = true;
    });
  }

  private coldObservableShareReplayUnsubscribe() {
    const observableFactory = () =>
      interval(2000).pipe(tap(() => this.logger.log('Interval emitted.')));

    const observable = new Observable((subscriber) => {
      this.logger.info('Cold observable created.');
      const o = observableFactory();
      o.subscribe(subscriber);

      return () => {
        this.logger.info('Cold observable unsubscribed successfully.');
      };
    }).pipe(
      shareReplay({
        bufferSize: 1,
        refCount: true,
      })
    );

    const subscription = observable.subscribe(() => {
      this.logger.log('Subscription callback called.');
    });

    timer(3500).subscribe(() => {
      this.logger.log('Going to unsubscribe');
      subscription.unsubscribe();
    });
  }

  private hotOrColdInterval() {
    const interval$ = interval(2000);

    interval$.subscribe(() => {
      this.logger.log('Interval 1', new Date().getTime());
    });

    setTimeout(() => {
      interval$.subscribe(() => {
        this.logger.log('Interval 2', new Date().getTime());
      });
    }, 750);
  }

  private coldObservableIllustration() {
    const observable$ = new Observable((observer) => {
      const interval$ = interval(1000);

      interval$.subscribe((value: number) => {
        observer.next(value);
      });
    });

    observable$.subscribe((value) => {
      this.logger.log('Subcription #1 - Received:', value);
    });

    setTimeout(() => {
      observable$.subscribe((value) => {
        this.logger.log('Subcription #2 - Received:', value);
      });
    }, 2500);
  }

  private hotObservableIllustration() {
    const subject$ = new Subject();

    interval(1000).subscribe((value: number) => {
      subject$.next(value);
    });

    const observable$ = new Observable((observer) => {
      subject$.subscribe((value: number) => {
        observer.next(value);
      });
    });

    observable$.subscribe((value) => {
      this.logger.log('Subcription #1 - Received:', value);
    });

    setTimeout(() => {
      observable$.subscribe((value) => {
        this.logger.log('Subcription #2 - Received:', value);
      });
    }, 2500);
  }

  private createHotObservable<T>(coldObservable: Observable<T>) {
    const subject$ = new Subject();
    const sourceSub = coldObservable.subscribe(subject$);
    let refCount = 0;

    return new Observable((observer) => {
      refCount++;
      const sub = subject$.subscribe(observer);

      // Returns teardown logic, i.e. function called when the observable is unsubscribed
      return () => {
        refCount--;

        sub.unsubscribe();

        if (refCount === 0) {
          sourceSub.unsubscribe();
        }
      };
    });
  }

  private repeatWhenTest() {
    of({
      statusCode: 307,
    })
      .pipe(
        tap((val) => {
          if (val.statusCode === 307) {
            throw new Error('Code is 307');
          }
        }),
        retryWhen((notifier) => {
          let counter = 10;
          return notifier.pipe(
            switchMap((err) => {
              this.logger.log('Notifier emitted', JSON.stringify(err));
              this.logger.log('Count:', counter);
              if (counter-- === 0) {
                return throwError(err);
              }

              return of(err).pipe(delay(1500));
            })
          );
        })
      )
      .subscribe(
        (res) => {
          this.logger.log('Result:', JSON.stringify(res));
        },
        (err: Error) => {
          this.logger.log('Error:', err.message);
        }
      );
  }
}
